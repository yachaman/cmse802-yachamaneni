#!/usr/bin/python
# -*- coding: utf-8 -*-

# Importing Python Pachages
import os
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from pylab import rcParams


sns.set()


def EDA(df):

    # convert the lastUpdated column into datetime format

    df['LastUpdated'] = pd.to_datetime(df['LastUpdated'])
    df['Date'] = df['LastUpdated'].dt.date
    df['Time'] = df['LastUpdated'].dt.time

    cols = ['SystemCodeNumber', 'Capacity', 'LastUpdated']
    df.drop(cols, axis=1, inplace=True)
    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)
    y = df['Occupancy'].resample('D').mean()
    y.fillna(method='bfill', inplace=True)

    return y


def is_trend_seasonality(data):
    """ This function takes pandas data series and returns a plot that shows
        different trend and seasonality
        presence inside the data."""

    if not os.path.isfile(data):
        raise ValueError("Input file does not exist: {0}. I'll quit now.".format(data))
    df_data = pd.read_csv(data)

    if df_data.shape[0] < 1000:
        raise ValueError('Not enough rows in input file.')

    if df_data.shape[1] < 4:
        raise ValueError('Not enough coloumns in input file.')

    k = df_data.columns == ['SystemCodeNumber', 'Capacity', 'Occupancy',
                       'LastUpdated']

    for i in k:
        if k == False:
            raise ValueError('Coloms are incorrect.')

    y = EDA(df_data)

    # y.plot(figsize=(15, 6))
    # plt.show()

    rcParams['figure.figsize'] = (12, 8)

    decomposition = sm.tsa.seasonal_decompose(y, model='additive')

    fig = decomposition.plot()

    plt.show()
