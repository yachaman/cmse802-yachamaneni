import numpy as np
import pandas as pd

# Plots
# ==============================================================================
import matplotlib.pyplot as plt
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.graphics.tsaplots import plot_pacf
import plotly.express as px
plt.style.use('fivethirtyeight')
plt.rcParams['lines.linewidth'] = 1.5

# Modelling and Forecasting
# ==============================================================================
from xgboost import XGBRegressor
# from lightgbm import LGBMRegressor
# from catboost import CatBoostRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from skforecast.ForecasterAutoreg import ForecasterAutoreg
from skforecast.ForecasterAutoregMultiOutput import ForecasterAutoregMultiOutput
from skforecast.model_selection import grid_search_forecaster
from skforecast.model_selection import backtesting_forecaster
from joblib import dump, load

# Configuration
# ==============================================================================
import warnings
warnings.filterwarnings('ignore')

class xg_boost():

    def interactive_plot(self,data):
        '''
        Input: Data
        Output: Plot the train-test-validation split
        '''
        end_train = '2012-03-31 23:59:00'
        end_validation = '2012-08-31 23:59:00'
        data_train = data.loc[: end_train, :]
        data_val   = data.loc[end_train:end_validation, :]
        data_test  = data.loc[end_validation:, :]
        # Plot time series
        # ==============================================================================
        fig, ax = plt.subplots(figsize=(11, 4))
        data_test['users'].plot(ax=ax, label='test')
        data_val['users'].plot(ax=ax, label='validation')
        data_train['users'].plot(ax=ax, label='train')
        ax.set_title('Number of users')
        ax.legend();
    def auto_corr(self,data):
        '''
        Input: Data
        Output: Auto Correlation Function
        '''
        # Autocorrelation plot
        # ==============================================================================
        fig, ax = plt.subplots(figsize=(7, 3))
        plot_acf(data['users'], ax=ax, lags=72) # Lags range
        plt.show()
          
    def partial_auto_corr(self,data):
        # Partial autocorrelation plot
        # ==============================================================================
        fig, ax = plt.subplots(figsize=(7, 3))
        plot_pacf(data['users'], ax=ax, lags=72, method='ywm')
        plt.show()
    
    def testing(self,forecaster,data,end_validation):
        '''
        Input: Forecasting object, end_validation
        Output: Metric and Forecaster
        
        This function overall sets the hyperparameters that XGBOOST uses to fine tune
        '''
        metric, predictions = backtesting_forecaster(
            forecaster = forecaster,
            y          = data['users'],
            initial_train_size = len(data.loc[:end_validation]),
            fixed_train_size   = False,
            steps      = 36,
            refit      = False,
            metric     = 'mean_squared_error',
            verbose    = False # Change to True to see detailed information
            )
        
        print(f"Backtest error: {metric}") 
        
        return [metric, predictions]
    
  
    def grid_search_xgboost(self, data,data_train,end_validation,lags_grid):
        '''
        Input: Training Data, Validation Data, Lags
        Output: Pandas Data Frame of best hyper parameters
        
        This function does grid search method to get best hyper parameters for the pre-defined range of parameters
        '''
        
        end_validatio = end_validation
        # Grid search of hyperparameters and lags
        # ==============================================================================
        # Regressor hyperparameters
        forecaster = ForecasterAutoreg(
                        regressor = XGBRegressor(random_state=123),
                        lags = 24
                        )
        param_grid = {
            'n_estimators': [100, 500],
            'max_depth': [3],
            'learning_rate': [0.01]
            }
        
        # Lags used as predictors
        #lags_grid = [24, 48, 72, [1, 2, 3, 23, 24, 25, 71, 72, 73]]
        
        from tqdm import tqdm
        from functools import partialmethod
        tqdm.__init__ = partialmethod(tqdm.__init__, disable=True)
        results_grid = grid_search_forecaster(
                forecaster         = forecaster,
                y                  = data.loc[:end_validation, 'users'], # Train and validation data
                param_grid         = param_grid,
                lags_grid          = lags_grid,
                steps              = 36,
                refit              = False,
                metric             = 'mean_squared_error',
                initial_train_size = int(len(data_train)), # Model is trained with trainign data
                fixed_train_size   = False,
                return_best        = True,
                verbose            = False
                )
        
            
        return [results_grid,forecaster]
    

    
    def forecast_viz(self,data_test,predictions):
        # Interactive plot of predictions
        # ==============================================================================
        data_plot = pd.DataFrame({
                        'test': data_test['users'],
                        'prediction': predictions['pred'],
                         })
        data_plot.index.name = 'date_time'
        
        fig = px.line(
                data_frame = data_plot.reset_index(),
                x      = 'date_time',
                y      = data_plot.columns,
                title  = 'Number of users',
                width  = 900,
                height = 500
              )
        
        fig.update_xaxes(rangeslider_visible=True)
        fig.show()
        
        
    def change_data(self,data):
        '''
        Input: Data
        Returns: Changed Data 
        
        This function adds the exogeneous variables like weather, month and weekday information
        that affects the number of bike user rentals each day.
        
        
        '''
        # Store categorical variables as category type
        # ==============================================================================
        data['weather'] = data['weather'].astype('category')
        data['month']   = data['month'].astype('category')
        data['weekday'] = data['weekday'].astype('category')
        # Transformation sine-cosine of variable hour
        # ==============================================================================
        data['hour_sin'] = np.sin(data['hour'] / 23 * 2 * np.pi)
        data['hour_cos'] = np.cos(data['hour'] / 23 * 2 * np.pi)
        data = data.drop(columns='hour')
        data = pd.get_dummies(data, columns=['weather', 'month', 'weekday'])
        return data
    
    def exogenious(self,data,end_train,end_validation):
        # Select exogenous variables, including those generated by one hot encoding.
        exog_variables = [column for column in data.columns
                              if column.startswith(('weather', 'month', 'hour', 'weekday'))]
        exog_variables.extend(['holiday', 'temp', 'atemp', 'hum', 'windspeed'])
        # Since data has been transformed, the train, val and test split is repeated.
        data_train = data.loc[: end_train, :]
        data_val   = data.loc[end_train:end_validation, :]
        data_test  = data.loc[end_validation:, :]    
        #changed_vals = grid_search_xgboost(data,data_train,end_validation,lags_grid)
        # grid_search_xgboost(data,data_train,end_validation,lags_grid)
        return [data_train,data_val,data_test]






    
    
