import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
import numpy as np
import pandas as pd
register_matplotlib_converters()
import seaborn as sns
sns.set(style="whitegrid")

from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
import warnings
warnings.filterwarnings('ignore')
RANDOM_SEED = np.random.seed(0)



# Importing necessary functions
# return Series of distance between each point and its distance with the closest centroid
def get_Dist_Point(df, knn_model):
    """ 
    Input: Dataframe df, knn_model object
    Output: Distance vector from centroind cluster to remaining clusters
    
    """
    dis = pd.Series()
    for i in range(0,len(df)):
        Xa = np.array(df.loc[i])
        Xb = knn_model.cluster_centers_[knn_model.labels_[i]-1]
        dis.at[i]=np.linalg.norm(Xa-Xb)
    return dis
# select only closing price
def close_price(df):
    """ 
    Takes the dataframe and returns the closing price of the data where the index is the data and time.
    """
    X = df[['close']]
    X = X.reset_index(drop=True)
    return X


def km_n_clusters (n_clusters,X):
    km= KMeans(n_clusters)
    km.fit(X)
    
def find_tr(outliers_fraction,distance):
    """
    Takes the distance and Outlier fraction and finds the longest distance clusters from the centroid node or cluster.
    """
    number_of_outliers = int(outliers_fraction*len(distance))
    threshold = distance.nlargest(number_of_outliers).min()
    return threshold

def plot_outliers(a,df,data):
    """
    Input: Anomaly Data -> a, dataframe df
    """
    fig, ax = plt.subplots(figsize=(10,6))
    ax.plot(df.index, data[0], color='k',label='Normal')
    ax.scatter(a.index,a[0], color='red', label='Anomaly')
    ax.xaxis_date()
    plt.xlabel('Date Time')
    plt.ylabel('price in USD')
    plt.legend()
    fig.autofmt_xdate()
    plt.show()
