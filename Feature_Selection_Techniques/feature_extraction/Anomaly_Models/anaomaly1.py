import numpy as np
import pandas as pd
import seaborn as sns
sns.set(style="whitegrid")
from sklearn.ensemble import IsolationForest
import matplotlib.pyplot as plt
from datetime import datetime
from pandas.plotting import register_matplotlib_converters
from sklearn.preprocessing import StandardScaler
import warnings
warnings.filterwarnings('ignore')
RANDOM_SEED = np.random.seed(1)

pd.set_option('display.max_rows', 25)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)

register_matplotlib_converters()




def parser(s):
    return datetime.strptime(s, '%d-%m-%Y')

# df = pd.read_csv('spx.csv', parse_dates=[0], index_col=0, date_parser=parser)
# load the data
df = pd.read_csv('spx.csv', parse_dates=['date'], index_col='date')
outliers_frac = float(.015)

scaler = StandardScaler()
np_scalar = scaler.fit_transform(df.values.reshape(-1, 1))
data = pd.DataFrame(np_scalar)

# train the isolation forest algorithm
model =  IsolationForest(contamination=outliers_frac)
model.fit(data) 

df['anomaly'] = model.predict(data)

# visualization of outliers
fig, ax = plt.subplots(figsize=(10,6))

A = df.loc[df['anomaly'] == -1, ['close']] #anomaly

ax.plot(df.index, df['close'], color='black', label = 'Normal')
ax.scatter(A.index,A['close'], color='red', label = 'Anomaly')
plt.legend()
plt.show();

