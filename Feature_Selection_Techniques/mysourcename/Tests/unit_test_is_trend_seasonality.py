import is_trend_seasonality
import pytest
import pandas as pd

def test_file_load_good():
    # test if function returns numpy array for good input file.
    data = is_trend_seasonality.pd.read_csv("dataset.csv")
    assert type(data) == pd.core.frame.DataFrame
        
def check_dimensions():
    with pytest.raises(ValueError) as excinfo:
        is_trend_seasonality.pd.read_csv("dataset2.csv")

    assert "file dimensitons dosent match" in str(excinfo.value)

def check_coloms():
    with pytest.raises(ValueError) as excinfo:
        is_trend_seasonality.pd.read_csv("dataset1.csv")

    assert "file coloms dosent match" in str(excinfo.value)
    


