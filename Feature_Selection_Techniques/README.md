 
# Goal, motivation, and questions
The data model that I choosed for the project is "Time Series". And overall goal of the project is:
1. Showing how powerfull the feature engineering is.
2. Describing problems associated Problems in creating feature engineering pipeline to time series data.
3. Describing problems associated Problems in creating Problems that even feature engineering cant solve
4. Describing problems associated Problems in creating New problems araised after feature engineering 




# Overall aim is to:

1. Find concepts and detect techniques related to:
2. Use Statistics and Probability
3. Use Deep Learning
4. Use Machine learning ( optional )
 



# Feature engineering techniques
1. Features with or without trends and seaonality
2. Features without outliers
3. Gradient Boosting

## Inside the feature_extraction folder, there are 2 sub folders and files that does some functions.

# Folder-1: Anomaly_Models, content includes
1. This folder has all the files and dataset for anomaly detection in the time series data.
2. anomaly.py: Code for K-Means Clustering algorithm
3. anomaly1.py: Code for Isolation Forest Algorithm
4. anomaly2.py: Code for LSTM, a RNN based algorithm
5. example_anomaly.ipynb: Has the code of all three algorithms on how to use them effectively with the necessary output.
6. spx.csv: Dataset

# Folder-2: Prediction_Models, content includes
1. This folder has all the files and dataset for prediction or Forecasting in the time series data.
2. rnn_anom.py: Code for LSTM algorithm in pytorch framework.
3. xgb2.py: Code for XGBOOST algorithm using sklearn and skforecast algorithms.
4. SBUX.csv: Dataset used for rnn_anom.py file
5. bike_sharing_dataset.csv: Dataset used for xgb2.py file
6. example_forecasting.ipynb: Has the code of all 2 forecasting algorithms on how to use them effectively with the necessary output.

# File-1: Final_Report.ipynb
1. This file has all the detailed work done in this project.

# File-2: environment.yml
1. This file has all the libraries and packages to be installed in prior.

## miscellaneous
# Function-1: is_trends_seasonality()
1. This function takes data and return a visualization.
2. Here user can decide the necessary functionality

# File-3: code_test.py
1. This file used the pytest library to check the code in  is_trends_seasonality() for the milestone-4.


